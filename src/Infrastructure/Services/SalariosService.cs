using System.Linq.Expressions;
using Application.Dtos.Entities;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using AutoMapper;
using Domain.Entities;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Services
{
    public class SalariosService : ISalariosService
    {
        private readonly ILogger<SalariosService> _logger;
        private readonly ISalariosRepository _salario;
        private readonly IMapper _mapper;
        public SalariosService(ILogger<SalariosService> logger,
                               ISalariosRepository salario,
                               IMapper mapper)
        {
            _logger = logger;
            _salario = salario;
            _mapper = mapper;
        }
        public async Task<bool> Add(SalariosDto entity)
        {
            try
            {
                 Salarios salario = _mapper.Map<Salarios>(entity);
                await _salario.Add(salario);
                
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<IEnumerable<SalariosDto>> GetAll()
        {
            return _mapper.Map<IEnumerable<Salarios>, IEnumerable<SalariosDto>>(await _salario.GetAll());
            
        }
    }
}