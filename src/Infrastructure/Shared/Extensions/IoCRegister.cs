using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Mappings;
using Infrastructure.Contexts;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Shared.Extensions
{
    public static class IoCRegister
    {
        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            //Contexts
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")))
                .AddRegisterRepository()
                .AddRegisterServices()
                .AddAutoMappers();
           
             
        }

         public static IServiceCollection AddRegisterRepository(this IServiceCollection services)
         {
             return services.AddTransient<ISalariosRepository, SalariosRepository>()               
                .AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>));
         }

         public static IServiceCollection AddRegisterServices(this IServiceCollection services)
         {
             return services.AddTransient<ISalariosService, SalariosService>();
             
         }
        private static IServiceCollection AddAutoMappers(this IServiceCollection services)
        {
            return services.AddAutoMapper(typeof(SalariosProfile));
            
        }


       
    }
}