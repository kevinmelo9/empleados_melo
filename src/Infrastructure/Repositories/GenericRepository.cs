using System.Linq.Expressions;
using Application.Interfaces.Repositories;
using Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly ApplicationContext _context;

        public GenericRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<bool> Add(T entity)
        {
             try
            {
                await _context.Set<T>().AddAsync(entity);
                await _context.SaveChangesAsync();

                return true;                
            }
            catch 
            {                
                return false;                
            }                           
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }
    }
}