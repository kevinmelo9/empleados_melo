using Application.Interfaces.Repositories;
using Domain.Entities;
using Infrastructure.Contexts;

namespace Infrastructure.Repositories
{
    public class SalariosRepository: GenericRepository<Salarios>, ISalariosRepository
    {
        public SalariosRepository(ApplicationContext context) : base(context)
        {
            
        }            
    }
}