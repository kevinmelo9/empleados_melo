using Domain.Entities;

using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Infrastructure.Contexts
{
    public class ApplicationContext: DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            
             
        }

        public DbSet<Salarios> Salarios {get; set;}
        public DbSet<Divisions> Divisions {get; set;}
        public DbSet<Offices> Offices {get; set;}
        public DbSet<Positions> Positions {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

                //Cargando los datos del archivo inicial
                var salarios = new List<Salarios>();
                using (StreamReader r = new StreamReader(@"C:\source\repos\temp\empleados\Infrastructure\Files\Data.json"))
                {
                    string json = r.ReadToEnd();
                    salarios = JsonConvert.DeserializeObject<List<Salarios>>(json);
                }
                
                modelBuilder.Entity<Salarios>().HasData( 
                    salarios
                ); 
                
                 
                  
                modelBuilder.Entity<Divisions>().HasData(
                    new { divisionid = 1, Nombre="OPERATION"},
                    new { divisionid = 2, Nombre="SALES"},
                    new { divisionid = 3, Nombre="MARKETING" },
                    new { divisionid = 4, Nombre="CUSTOMER CARE" }                    
                );
            
                modelBuilder.Entity<Offices>().HasData(
                    new { officeid = 1, Nombre="SAN DIEGO"},
                    new { officeid = 2, Nombre="SANTIAGO"},
                    new { officeid = 3, Nombre="PUNTA CANA" },
                    new { officeid = 4, Nombre="SAN JUAN" } ,  
                    new { officeid = 5, Nombre="ZZZZZZ" },
                    new { officeid = 6, Nombre="SAAAAA" }                                     
                );
                modelBuilder.Entity<Positions>().HasData(
                    new { positionid = 1, Nombre="CARGO MANAGER"},
                    new { positionid = 2, Nombre="HEAD OF CARGO"},
                    new { positionid = 3, Nombre="CARGO ASSISTANT" },
                    new { positionid = 4, Nombre="SALES MANAGER" },                    
                    new { positionid = 5, Nombre="ACCOUNT EXECUTIVE"},
                    new { positionid = 6, Nombre="MARKETING ASSISTANT" }
                
                );
        }
    }
}