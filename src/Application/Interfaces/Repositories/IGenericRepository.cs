using System.Linq.Expressions;

namespace Application.Interfaces.Repositories
{
    public interface IGenericRepository<T> where T : class
    {       
        Task<bool> Add(T entity);
        Task<IEnumerable<T>> GetAll();                
    }
}