using Domain.Entities;

namespace Application.Interfaces.Repositories
{
    public interface ISalariosRepository: IGenericRepository<Salarios>
    {
         
    }
}