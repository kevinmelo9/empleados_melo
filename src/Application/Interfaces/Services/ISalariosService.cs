using System.Linq.Expressions;
using Application.Dtos.Entities;
 

namespace Application.Interfaces.Services
{
    public interface ISalariosService
    {        
        Task<bool> Add(SalariosDto entity);
        Task<IEnumerable<SalariosDto>> GetAll();       

    }
}