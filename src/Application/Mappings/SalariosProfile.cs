using Application.Dtos.Entities;
using AutoMapper;
using Domain.Entities;

namespace Application.Mappings
{
    public class SalariosProfile : Profile
    {
        public SalariosProfile()
        {
            //SalarioDto
            CreateMap<SalariosDto, Salarios>().ReverseMap();
            CreateMap<Salarios, SalariosDto>().ReverseMap(); 
              
            //SalarioRDto
            CreateMap<SalariosRDto, Salarios>().ReverseMap();
            CreateMap<Salarios, SalariosRDto>().ReverseMap();   
        }        
    }
}