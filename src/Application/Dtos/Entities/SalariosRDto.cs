namespace Application.Dtos.Entities
{
    public class SalariosRDto
    {
         
        public string Employee_Code { get; set; }        
        public string Employee_Name { get; set; }        
        public string Employee_Surname { get; set; }
        public int Division { get; set; }
        public int Position { get; set; }
        public int Grade { get; set; }
        public DateTime Begin_Date { get; set; }
        public DateTime Birthday { get; set; }
        public string Identification_Number { get; set; }
        public decimal Salary { get; set; }
        
    }
}