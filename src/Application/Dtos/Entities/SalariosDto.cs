using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Application.Dtos.Entities
{
    public class SalariosDto
    {
        public int id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Office { get; set; }     
        public string Employee_Code { get; set; }        
        public string Employee_Name { get; set; }        
        public string Employee_Surname { get; set; }
        public int Division { get; set; }
        public int Position { get; set; }
        public int Grade { get; set; }
        public DateTime Begin_Date { get; set; }
        public DateTime Birthday { get; set; }
        public string Identification_Number { get; set; }
        public decimal Base_Salary { get; set; }
        public decimal Production_Bonus { get; set; }
        public decimal Compensation_Bonus { get; set; }        
        public decimal Commission { get; set; }        
        public decimal Contributions { get; set; }
    }
}