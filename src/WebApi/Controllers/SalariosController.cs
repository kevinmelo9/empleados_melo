using Application.Dtos.Entities;

using Application.Interfaces.Services;
using Domain.Entities;
using Infrastructure.Helper;
using Microsoft.AspNetCore.Mvc;
using WebApi.Controllers.Base;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class SalariosController: BaseController
    {
         private readonly ISalariosService _salario;

        public SalariosController(ISalariosService salario)
        {
            _salario = salario;
        }

        // POST: /Salarios
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]SalariosDto salario, int Periodos_Agregar,int Apartir_Year)
        {
            if (ModelState.IsValid)
            {

                
                //Validaciones de Fechas
                if (Validate.ValidateDate(salario.Begin_Date.ToString()) !=true)
                    return BadRequest("Begin_Date no es un fecha Valida");
                
                 if (Validate.ValidateDate(salario.Birthday.ToString()) !=true)
                     return BadRequest("Birthday no es un fecha Valida");

                //Validando TExt
                if(string.IsNullOrEmpty(salario.Employee_Code) && salario.Employee_Code.Length > 10)
                    return BadRequest("EL Employee_Code es requerido y tener una longitud menor a 10");

                if(string.IsNullOrEmpty(salario.Employee_Name) && salario.Employee_Name.Length > 150)
                    return BadRequest("EL Employee_Name es requerido y tener una longitud menor a 150");

                if(string.IsNullOrEmpty(salario.Employee_Surname) && salario.Employee_Surname.Length > 150)
                    return BadRequest("EL Employee_Surname es requerido y tener una longitud menor a 150");

                if(string.IsNullOrEmpty(salario.Identification_Number) && salario.Identification_Number.Length > 10)
                    return BadRequest("EL Employee_Surname es requerido y tener una longitud menor a 10");

                //Validando Valores NUmericos
                if(salario.Year<=0)
                    return BadRequest("EL Year es requerido");

                if(salario.Month<=0)
                    return BadRequest("EL Month es requerido");

                if(salario.Office<=0)
                    return BadRequest("EL Office es requerido");

                if(salario.Division<=0)
                    return BadRequest("EL Division es requerido");

                if(salario.Position<=0)
                    return BadRequest("EL Position es requerido");

                if(salario.Grade<=0)
                    return BadRequest("EL Grade es requerido");

                if(salario.Base_Salary<=0)
                    return BadRequest("EL Base_Salary es requerido");

                if(salario.Production_Bonus<=0)
                    return BadRequest("EL Production_Bonus es requerido");

                 if(salario.Compensation_Bonus<=0)
                     return BadRequest("EL Compensation_Bonus es requerido");

                if(salario.Commission<=0)
                    return BadRequest("EL Commission es requerido");

                if(salario.Contributions<=0)
                    return BadRequest("EL Contributions es requerido");
 
    
                var resul = _salario.GetAll().Result.ToList();


                if (Periodos_Agregar<=0)
                {

                    //Validar que no dupliquen el anio y el mes en un mismo empleado                    
                    if (!ValidarPeriodosDuplicados(resul,salario.Employee_Code,salario.Year,salario.Month))
                        return BadRequest("No puede duplicar el mes y el año al mismo empleado");
                
                
                    
                    //Validar que no dupliquen el Nombre completo del empleado con otro Codigo anio y el mes en un mismo empleado
                    if (!ValidarNombresDuplicados(resul, salario.Employee_Code,salario.Employee_Name,salario.Employee_Surname))                            
                        return BadRequest("No puede duplicar el nombre completo del empleado en otro codigo");
                    
                    
            
                    if (await _salario.Add(salario))
                        return Created("Salario Creado",null);

                }
                else //Crear los nuevos periodos
                {
                     //Validar que no dupliquen el Nombre completo del empleado con otro Codigo anio y el mes en un mismo empleado
                    if (!ValidarNombresDuplicados(resul, salario.Employee_Code,salario.Employee_Name,salario.Employee_Surname))                            
                        return BadRequest("No puede duplicar el nombre completo del empleado en otro codigo");
                    
 
                    
                    Salarios newsalario = new Salarios();
                    int year = salario.Year;
                    int month = salario.Month;

                    for (int cant = 1; cant<= Periodos_Agregar; cant++)
                    {
validate1:
                          //Validar que no dupliquen el anio y el mes en un mismo empleado                    
                        if (!ValidarPeriodosDuplicados(resul,  salario.Employee_Code,year,month))
                        {
                            month+=1;
                            if (month>12)
                                {
                                    month=1; 
                                    year+=1;
                                }
                                goto validate1;
                        }
                
                        await _salario.Add(new SalariosDto
                        {
                            Year=year,
                            Month=month,
                            Office=salario.Office,
                            Employee_Code=salario.Employee_Code,
                            Employee_Name=salario.Employee_Name,
                            Employee_Surname=salario.Employee_Surname,
                            Division=salario.Division,
                            Position=salario.Position,
                            Grade=salario.Grade,
                            Begin_Date=salario.Begin_Date,
                            Birthday=salario.Birthday,
                            Identification_Number=salario.Identification_Number,
                            Base_Salary=salario.Base_Salary,
                            Production_Bonus=salario.Production_Bonus,
                            Compensation_Bonus=salario.Compensation_Bonus,
                            Commission=salario.Commission,
                            Contributions=salario.Contributions

                        });

                        month+=1;
                        if (month>12)
                        {
                            month=1; 
                            year+=1;
                        }
                    }
                }

                return Created("Salarios Creado",null);
            }
            else
            {
                return BadRequest();
            }
        }
        bool ValidarPeriodosDuplicados(List<SalariosDto> r, string code,int year,int month)
        {   
            //Validar que no dupliquen el anio y el mes en un mismo empleado                    
            if (r.Any(m => m.Employee_Code == code 
                            && m.Year == year
                            && m.Month == month))
                            return false;

            return true;
        }
        bool ValidarNombresDuplicados(List<SalariosDto> r, string code,string name,string surname)
        {   
            //Validar que no dupliquen el Nombre completo del empleado con otro Codigo anio y el mes en un mismo empleado
            if ( r.Any(m => m.Employee_Name == name 
                            && m.Employee_Surname == surname                                    
                            && m.Employee_Code != code))     
                            return false;
            return true;                       
        }

        // GET: /Salarios/5
        [HttpGet]
        public async Task<IEnumerable<SalariosRDto>> GetDatosTabla()
        {
            var resul = _salario.GetAll().Result.ToList();
            
            IEnumerable<SalariosRDto> Result = (from q in resul
                        group q by q.Employee_Code into g
                        orderby g.Key
                        select new SalariosRDto
                        {
                            Employee_Code = g.First().Employee_Code,
                            Employee_Name = g.First().Employee_Name,
                            Employee_Surname = g.First().Employee_Surname,
                            Division = g.Last().Division,
                            Position = g.Last().Position,
                            Grade = g.Last().Grade,
                            Begin_Date = g.Last().Begin_Date,
                            Birthday = g.Last().Birthday,
                            Identification_Number = g.Last().Identification_Number,                            
                            Salary = ((g.Last().Base_Salary + g.Last().Production_Bonus +
                                             (g.Last().Compensation_Bonus * Convert.ToDecimal(0.75)) 
                                             + ((g.Last().Base_Salary + g.Last().Commission) * Convert.ToDecimal(0.08)) + g.Last().Commission)
                                             - g.Last().Contributions)
                                            
                        }).AsEnumerable();           
                
            return await Task.FromResult(Result);
        }

        [HttpGet("{Employee_Code}/{btn}")]
        public async Task<ActionResult> GetDatosTablaFiltrada(string Employee_Code,int btn)
        {            
            IEnumerable<SalariosRDto> Result ;

            if (btn < 1 ||  btn > 4)
            {
                return BadRequest("La opcion de busqueda no esta definida.");
            }

            if (string.IsNullOrEmpty(Employee_Code))
            {
                return BadRequest("Debe especificar un empleado o una opcion de Busqueda.");
            }

            var resul = await _salario.GetAll();

            var Valores = resul.Where(x => x.Employee_Code == Employee_Code)
                                .GroupBy(x => x.Employee_Code)
                                .Select(x => new 
                                {
                                    office =  x.Last().Office,
                                    grade = x.Last().Grade,
                                    positions = x.Last().Position,
                                    month = x.Last().Month
                                });
                                
        // Empleados con la misma Oficina y Grado.
        // - Empleados de todas las Oficinas y con el mismo Grado.
        // - Empleados con la misma Posición y Grado.
        // - Empleados con el mismo Grado y el mismo mes de Ingreso.

            Result = (from q in resul
                where  
                (btn == 1? q.Office == Valores.First().office && q.Grade == Valores.First().grade:
                btn == 2? q.Grade == Valores.First().grade:                        
                btn == 3? q.Position == Valores.First().positions  && q.Grade == Valores.First().grade:
                btn == 4? q.Grade == Valores.First().grade && q.Month == Valores.First().month:
                q.Employee_Code==q.Employee_Code)
                group q by q.Employee_Code into g
                orderby g.Key
                select new SalariosRDto
                {
                    Employee_Code = g.First().Employee_Code,
                    Employee_Name = g.First().Employee_Name,
                    Employee_Surname = g.First().Employee_Surname,
                    Division = g.Last().Division,
                    Position = g.Last().Position,
                    Grade = g.Last().Grade,
                    Begin_Date = g.Last().Begin_Date,
                    Birthday = g.Last().Birthday,
                    Identification_Number = g.Last().Identification_Number,
                    Salary = ((g.Last().Base_Salary + g.Last().Production_Bonus +
                                             (g.Last().Compensation_Bonus * Convert.ToDecimal(0.75)) 
                                             + ((g.Last().Base_Salary + g.Last().Commission) * Convert.ToDecimal(0.08)) + g.Last().Commission)
                                             - g.Last().Contributions)
                }).AsEnumerable();           
                    
            return await Task.FromResult(Ok(Result));
            
            
            
            
        }        

        [HttpGet("{EmployeeCode}")]
        public async Task<IEnumerable<SalariosRDto>> GetDatosFiltrada(string EmployeeCode)
        {            
            var resul = _salario.GetAll().Result.ToList();
            
            IEnumerable<SalariosRDto> Result = (from q in resul                                                
                        where q.Employee_Code == EmployeeCode
                        select new SalariosRDto
                        {
                            Employee_Code = q.Employee_Code,
                            Employee_Name = q.Employee_Name,
                            Employee_Surname = q.Employee_Surname,
                            Division = q.Division,
                            Position = q.Position,
                            Grade = q.Grade,
                            Begin_Date = q.Begin_Date,
                            Birthday = q.Birthday,
                            Identification_Number = q.Identification_Number,
                            Salary = q.Base_Salary
                        }).AsEnumerable();           

            return await Task.FromResult(Result);
            
            
        }        
    }
}