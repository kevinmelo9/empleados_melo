using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Divisions
    {
         [Key]
        public int divisionid { get; set; }
        public string  Nombre { get; set; }
    }
}