using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Positions
    {
        [Key]
        public int positionid { get; set; }
        public string  Nombre { get; set; }
    }
}