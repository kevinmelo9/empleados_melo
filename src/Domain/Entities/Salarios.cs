using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Salarios
    {
        [Key]
        public int id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Office { get; set; }
        // [StringLength(10)]
        [Column (TypeName ="nvarchar(10)")]
        public string Employee_Code { get; set; }
        // [StringLength(150)]
        [Column (TypeName ="nvarchar(150)")]
        public string Employee_Name { get; set; }
        // [StringLength(150)]
        [Column (TypeName ="nvarchar(150)")]
        public string Employee_Surname { get; set; }
        public int Division { get; set; }
        public int Position { get; set; }
        public int Grade { get; set; }
        public DateTime Begin_Date { get; set; }
        public DateTime Birthday { get; set; }

        // [StringLength(10)]
        [Column (TypeName ="nvarchar(10)")]
        public string Identification_Number { get; set; }
        [Column (TypeName ="decimal(18,2)")]
        public decimal Base_Salary { get; set; }
        [Column (TypeName ="decimal(18,2)")]
        public decimal Production_Bonus { get; set; }
        [Column (TypeName ="decimal(18,2)")]
        public decimal Compensation_Bonus { get; set; }
        [Column (TypeName ="decimal(18,2)")]
        public decimal Commission { get; set; }
        [Column (TypeName ="decimal(18,2)")]
        public decimal Contributions { get; set; }
    }
}