# Instalacion de Proyecto
## Instalar Dotnet Core
1. Ir a la Pagina de Dotnet Core [Dotnet Core](https://dotnet.microsoft.com/download/dotnet)
2. Descargar la version NET 6.0 acorde con tu OS.

## Clonar el repositorio
1. Ir a la ruta donde se ubicará el proyecto.
2. Abra una terminal/Consola y ejecute el siguiente comando:
>git clone git@bitbucket.org:kevinmelo9/empleados_melo.git

## Ejecutar el proyecto de forma independiente
1. Vaya a la carpeta WebApi.
2. Ejecute el siguiente comando:
>dotnet run
 
## Ejecutar la prueba
1. Vaya a la carpeta Empleados.Test ubicada fuera de la carpeta src.
2. Ejecute el comando:
> prueba dotnet

# Para la Interfaz realizada en aspx.net
## Abrir el proyeto 
1. Cambiar la url del Api en el metodo FillGrid
1. Ejecutarlo

 
NOTA 1: Este proyecto necesita una instancia de SQL Server ejecutándose. La aplicación creará automáticamente la base de datos y la cargara los datos desde el archivo "DATA.json" ubicado en la carpeta "Infrastructure/Files".

NOTA 2: asegúrese de cambiar la información de la cadena de conexión en el archivo *appsettings.json* ubicado en la carpeta WebApi.