﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppEmpleados.Models;

namespace WebAppEmpleados
{
    public partial class _Default : Page
    {
        IEnumerable<SalariosRDto> salario = null;
        Decimal AcumuladoSalario = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            datagrid.DataSource = salario;
            datagrid.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

     
        public void FillGrid()
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodigoEmpleado.Text))
                {
                    salario = null;
                    datagrid.DataSource = null;
                    txtBonoEmpleado.Text = "0.00";
                    return;
                }
                if (salario == null || salario.Count() <= 0 || salario.First().Employee_Code != txtCodigoEmpleado.Text)
                {
                    AcumuladoSalario = 0;
                    HttpClient hc = new HttpClient();
                    hc.BaseAddress = new Uri("https://localhost:7110/");

                    var consumeapi = hc.GetAsync("api/salarios/" + txtCodigoEmpleado.Text);
                    consumeapi.Wait();

                    var readdata = consumeapi.Result;
                    if (readdata.IsSuccessStatusCode)
                    {
                        var displayrecords = readdata.Content.ReadAsAsync<IList<SalariosRDto>>();
                        displayrecords.Wait();

                        salario = displayrecords.Result;
                        datagrid.DataSource = salario;
                        datagrid.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[6].Text = Convert.ToDateTime(e.Row.Cells[6].Text.Substring(0,10)).ToString("dd/MM/yyyy");
                e.Row.Cells[7].Text = Convert.ToDateTime(e.Row.Cells[7].Text.Substring(0, 10)).ToString("dd/MM/yyyy");
                
                if ((e.Row.RowIndex) >= salario.Count() - 3)
                {
                    e.Row.BackColor = System.Drawing.Color.Yellow;
                    AcumuladoSalario += Convert.ToDecimal(e.Row.Cells[9].Text);

                    if (e.Row.RowIndex == salario.Count() - 1)
                    {
                        txtBonoEmpleado.Text = Convert.ToDecimal(AcumuladoSalario / 3).ToString("###,###,##0.00");
                    }

                }
                
            }
        }

    }
}