﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAppEmpleados._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="justify-content:center">
        <div  style="justify-content:center">
            <br />
           <table>  
                <tr>  
                    <td>  
                        <asp:Label ID="lblEmp" runat="server" Text="Codigo Empleado"></asp:Label>                         
                    </td>  
                    <td>  
                        
                    </td>                           
                </tr> 
               <tr>  
                    <td>
                        <asp:TextBox ID="txtCodigoEmpleado" runat="server"></asp:TextBox>  
                    </td>  
                    <td>  
                        <asp:Button ID="btnBuscar" runat="server" Text="Button" OnClick="btnBuscar_Click" />
                    </td>                           
                </tr>  
            </table>  
            <br />
        </div>
        <div class="row" style="text-align: center">
             <div>  
                 <asp:GridView ID="datagrid" runat="server" CssClass="mydatagrid" PagerStyle-CssClass="pager"
                    HeaderStyle-CssClass="header" RowStyle-CssClass="rows" AllowPaging="True" 
                      OnRowDataBound="OnRowDataBound">
                 </asp:GridView>                
            </div>
        </div>

         <br />
           <table>  
                <tr>  
                    <td>  
                        <asp:Label ID="Label1" runat="server" Text="Bono del Empleado"></asp:Label>                         
                    </td>                      
                </tr> 
               <tr>  
                    <td>
                        <asp:TextBox ID="txtBonoEmpleado" runat="server" CssClass="text-right" ReadOnly="True">0.00</asp:TextBox>  
                    </td>                      
                </tr>  
            </table>  
         
    </div>

</asp:Content>
