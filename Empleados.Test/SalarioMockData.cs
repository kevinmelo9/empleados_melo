using System;
using System.Collections.Generic;
using Application.Dtos.Entities;

namespace Empleados.Test
{
    public class SalarioMockData
    {
        public static List<SalariosDto> GetSalarios()
        {
        
        
            return  new List<SalariosDto>()
            {
                new SalariosDto() 
                {
                    Year = 2022,
                    Month = 1,
                    Office = 3,
                    Employee_Code = "99999999",
                    Employee_Name = "Nombre",
                    Employee_Surname = "Apellido",
                    Division = 2,
                    Position = 3,
                    Grade = 18,
                    Begin_Date = DateTime.Now,
                    Birthday = DateTime.Now,
                    Identification_Number = "222321223",
                    Base_Salary = 1700,
                    Production_Bonus = 1000,
                    Compensation_Bonus = 166,
                    Commission = 122,
                    Contributions = 322
                },

                new SalariosDto() 
                {
                    Year = 2022,
                    Month = 1,
                    Office = 3,
                    Employee_Code = "99988999",
                    Employee_Name = "Nombreee",
                    Employee_Surname = "Apellido",
                    Division = 2,
                    Position = 3,
                    Grade = 18,
                    Begin_Date = DateTime.Now,
                    Birthday = DateTime.Now,
                    Identification_Number = "222321223",
                    Base_Salary = 1700,
                    Production_Bonus = 1000,
                    Compensation_Bonus = 166,
                    Commission = 122,
                    Contributions = 322
                    },

                new SalariosDto() 
                {
                    Year = 2022,
                    Month = 1,
                    Office = 3,
                    Employee_Code = "99997799",
                    Employee_Name = "Nombeeessre",
                    Employee_Surname = "Apellido",
                    Division = 2,
                    Position = 3,
                    Grade = 18,
                    Begin_Date = DateTime.Now,
                    Birthday = DateTime.Now,
                    Identification_Number = "222321223",
                    Base_Salary = 1700,
                    Production_Bonus = 1000,
                    Compensation_Bonus = 166,
                    Commission = 122,
                    Contributions = 322
                }        
            };
        }

        public static SalariosDto NewSalario()
        {        
            return  new SalariosDto()
            {                
                Year = 2022,
                Month = 1,
                Office = 3,
                Employee_Code = "99999999",
                Employee_Name = "Nombre",
                Employee_Surname = "Apellido",
                Division = 2,
                Position = 3,
                Grade = 18,
                Begin_Date = DateTime.Now,
                Birthday = DateTime.Now,
                Identification_Number = "222321223",
                Base_Salary = 1700,
                Production_Bonus = 1000,
                Compensation_Bonus = 166,
                Commission = 122,
                Contributions = 322                        
            };
        }
    }
}