using System;
using System.Threading.Tasks;
using Application.Dtos.Entities;
using Application.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WebApi.Controllers;
using Xunit;
namespace Empleados.Test
{
    public class SalariosControllerTest
    {       
        
        [Theory]
        [InlineData("99999999", 1)]
        [InlineData("99999999", 2)]
        [InlineData("99999999", 3)]
        [InlineData("99999999", 4)]    
        public async void Deberia_Retornar_Datos_Empleados_Con_Las_Diferentes_Modalidades(string EmployeeCode,int option)
        {
            // Given
            var mockRepo = new Mock<ISalariosService>();        
            mockRepo.Setup(_ => _.GetAll()).ReturnsAsync(SalarioMockData.GetSalarios());
               
            var controller = new SalariosController(mockRepo.Object);

            // When
            var salarios = await controller.GetDatosTablaFiltrada(EmployeeCode,option);
        
            // Then
            var result = (OkObjectResult)salarios;       
                
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
        }

        [Theory]
        [InlineData(0,StatusCodes.Status201Created)]            
        [InlineData(1,StatusCodes.Status201Created)]            
        public async void Deberia_Agregar_Nuevo_Empleado(int option, int expectedCode)
        {
            //Given
            object actionResult;
            SalariosDto salario = new SalariosDto();
        
            var mockRepo = new Mock<ISalariosService>();        
            mockRepo.Setup(repo => repo.Add(salario)).Returns(Task.FromResult<bool>).Verifiable();
            
            var controller = new SalariosController(mockRepo.Object);
                
            salario = SalarioMockData.NewSalario();
        
            //When   
            if (option == 0)
            {
                actionResult = await controller.Add(salario,0,0);
            } 
            else
            {
                actionResult = await controller.Add(salario,4,2022);
            }
            
    
            //Then
            var result = (CreatedResult)actionResult;                    
            Assert.Equal(expectedCode, result.StatusCode);
        }

         
    }
}